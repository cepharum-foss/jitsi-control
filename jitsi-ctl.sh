#!/bin/bash

PWD="$(pwd)"


INSTALLDIR="${INSTALLDIR:-}"

if [ -z "$INSTALLDIR" ]; then
        while read CANDIDATE; do
                DIR="$(dirname "$CANDIDATE")"

                if [ -f "$DIR/gen-passwords.sh" -a -f "$DIR/docker-compose.yml" ]; then
                        if [ -z "$INSTALLDIR" ]; then
                                INSTALLDIR="$DIR"
                        else
                                echo "multiple install folders found, please provide INSTALLDIR" >&2
                                exit 1
                        fi
                fi
        done < <(find -name jigasi.yml)

        if [ -z "$INSTALLDIR" ]; then
                echo "missing install folder of jitsi-docker-meet" >&2
                exit 1
        fi
fi


NOW="$(date +%Y-%m-%dT%H:%M:%S)"

CONFIGDIR="$(awk -F = '$1=="CONFIG"{print$2}' "$INSTALLDIR/.env" | sed 's#~#'"$HOME"'#')"



cd "$INSTALLDIR"


backupConfig() {
        cp -pr "$CONFIGDIR" "$CONFIGDIR.$NOW"
        cp -p ".env" ".env.$NOW"
        diff -C 3 "env.example" ".env" >".env.$NOW.diff"
}

upgradeRepo() {
        if [ -d ".git" ]; then
                git pull
        fi
}

resetFiles() {
        rm -Rf "$CONFIGDIR"
        mkdir -p "$CONFIGDIR"/{web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}
        cp -pr "$CONFIGDIR.$NOW/web/letsencrypt" "$CONFIGDIR/web"
}

resetConfig() {
        cp "env.example" ".env"
        patch ".env" <".env.$NOW.diff"
        ./gen-passwords.sh
        resetFiles
}

dcRun() {
        FILES=

        if [ -n "$(grep -E "^JIGASI_SIP_URI" ".env" 2>/dev/null)" ]; then
                FILES="$FILES -f jigasi.yml"
        fi

        if [ -n "$(grep -E "^ENABLE_RECORDING" ".env" 2>/dev/null)" ]; then
                FILES="$FILES -f jibri.yml"
        fi

        if [ -n "$(grep -E "^ETHERPAD_URL_BASE" ".env" 2>/dev/null)" ]; then
                FILES="$FILES -f etherpad.yml"
        fi

        docker-compose -f docker-compose.yml $FILES "$@"
}

destroy() {
        dcRun down -v --remove-orphans "$@"
}

start() {
        dcRun up -d "$@"
}

stop() {
        dcRun down "$@"
}

ps() {
        dcRun ps "$@"
}


OPERATION="${1}"
shift

if [ -z "$OPERATION" ]; then
        echo "usage: $0 <command> [ <args> ]" >&2
        exit 1
fi

case "$OPERATION" in
        update)
                destroy && \
                backupConfig && \
                resetFiles && \
                start
                ;;

        edit)
                destroy && \
                backupConfig && \
                resetFiles && \
                nano ".env" && \
                start
                ;;

        upgrade)
                echo 1
                destroy && \
                echo 2
                backupConfig && \
                echo 3
                upgradeRepo && \
                echo 4
                dcRun pull && \
                echo 5
                resetConfig && \
                echo 6
                resetFiles && \
                echo 7
                nano ".env" && \
                echo 8
                start
                echo 9
                ;;

        stop)
                stop "$@"
                ;;

        ps)
                ps "$@"
                ;;

        destroy)
                destroy "$@"
                ;;

        start)
                start "$@"
                ;;

        *)
                dcRun "$OPERATION" "$@"
                ;;
esac

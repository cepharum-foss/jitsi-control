# Jitsi Control Script

shell script for controlling docker-based Jitsi setup

## License

MIT

## About

This project provides a shell script suitable for controlling a docker-based installation of Jitsi. It includes suggestions for best-practice on upgrading containers to latest version available. Contributions are welcome.

### Limitations

* The script has been written in context of an existing setup. Thus, it doesn't support fresh installation of [docker-based Jitsi](https://github.com/jitsi/docker-jitsi-meet) as of now. You should stick to the [official documentation](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker) for that.

## Usage

Download the script [jitsi-ctl.sh](jitsi-ctl.sh) to your server running docker-based setup of Jitsi. Adjust permissions to make it executable.

```bash
curl https://gitlab.com/cepharum-foss/jitsi-control/-/raw/master/jitsi-ctl.sh?inline=false
chmod +x jitsi-ctl.sh
```

After that the script may be invoked with the desired operation in first argument and one or more parameters for that operation in additional arguments:

```bash
./jitsi-ctl.sh <operation>
```

The script is searching current folder for the locally cloned repository of [docker-jitsi-meet](https://github.com/jitsi/docker-jitsi-meet) picking the first match. You may choose a particular installation using environment variable **INSTALLDIR**.

```bash
INSTALLDIR=/path/to/repo ./jitsi-ctl.sh <operation>
```

### Convenience Support

Unless using some special command of this script, all unknown commands are forwarded to docker-compose for running against the proper set up YAML definition files.

So you can restart your stack with:

```bash
./jitsi-ctl.sh restart
```

Logs of a particular service can be inspected with:

```bash
./jitsi-ctl.sh logs -f jigasi
```

### Upgrade

The most important special command of this script is

```bash
./jitsi-ctl.sh upgrade
```

This multi-step process is 

* stopping an existing stack, 
* backing up its configuration, 
* re-discovering changes made to the .env file,
* pulling latest version of repository,
* creating new configuration from updated example,
* running text editor for checking result of patching example configuration for recovering settings of previously running stack,
* resetting filesystem hierarchy shared with containers and
* starting stack eventually.
